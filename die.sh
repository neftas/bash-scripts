#!/bin/bash

die() {
    echo >&2 "$(basename "$0"): $1"
    exit "$2"
}
