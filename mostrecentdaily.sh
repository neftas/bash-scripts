#!/bin/bash -xv
#
# Get the date of the most recent daily report.

set -o nounset
set -o errexit

################################################################################
# Get the date of the most recent daily report.
# Globals:
#   None
# Arguments:
#   $1  The directory to look for daily reports.
# Returns:
#   The date of the the most recent daily report.
################################################################################
get_most_recent_date_from_report() {
    DAILY_REPORTS_DIR=$1
    LAST="2017-01-01"
    for abspath in "$DAILY_REPORTS_DIR"/*; do
        filename="$(basename "$abspath")"
        date_file="${filename%%.*}"
        if [[ $(date --date="$LAST" +%s) -lt $(date --date="$date_file" +%s) ]]; then
            LAST="$date_file"
        fi
    done
    echo "$LAST"
}
