#!/bin/bash
#
# Library for error handling.

################################################################################
# Prints error message to stderr.
# Globals:
#   None
# Arguments:
#   $1  The message to be printed to stderr.
# Returns:
#   None
################################################################################
err() {
    echo >&2 "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $@" >&2
}
