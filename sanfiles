#!/bin/bash
# sanfiles -- sanitize filenames by lowercasing them and replacing
# whitespace with underscores

set -o errexit
set -o nounset

for path in $(pwd)/*; do
    if [ ! -f "$path" ]; then
        continue
    fi
    # strip leading path
    leading="$(dirname "$path")"
    filename="$(basename "$path")"
    # to lowercase, even if filename is UTF-8
    replacement="${filename,,}"
    # replace spaces with underscores
    replacement="${replacement// /_}"
    if [ ! "$leading/$replacement" == "$path" ]; then
        mv -vi "$path" "$leading/$replacement"
    fi
done
